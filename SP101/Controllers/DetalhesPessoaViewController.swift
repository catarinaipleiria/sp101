//
//  DetalhesPessoaViewController.swift
//  SP101
//
//  Created by Catarina Silva on 08/03/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import UIKit

class DetalhesPessoaViewController: UIViewController {

    @IBOutlet weak var nomeLabel: UILabel!
    
    @IBOutlet weak var nacionalidadeLabel: UILabel!
    
    var pessoa = Pessoa(primeiroNome: " ", ultimoNome: " ", nacionalidade: " ")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        nomeLabel.text = pessoa.primeiroNome + " " + pessoa.ultimoNome
        nacionalidadeLabel.text = pessoa.nacionalidade
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
