//
//  PessoaViewController.swift
//  SP101
//
//  Created by Catarina Silva on 08/03/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import UIKit

class PessoaViewController: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var pessoa:Pessoa?
    var index:Int?
    
    @IBOutlet weak var primeiroNomeField: UITextField!
    @IBOutlet weak var ultimoNomeField: UITextField!
    @IBOutlet weak var nacionalidadeField: UITextField!
    
    @IBAction func guardarPessoa(_ sender: UIButton) {
        //devem proteger as String? com unwrapping
        
        if let p = pessoa {
            //alterar pessoa
            p.primeiroNome = primeiroNomeField.text!
            p.ultimoNome = ultimoNomeField.text!
            p.nacionalidade = nacionalidadeField.text!
            SP1010Repository.repository.pessoas[index!] = p
            displayAlert(message: "Pessoa alterada", buttonTitle: "OK", vc: self)
        } else { //adicionar nova pessoa
            if primeiroNomeField.text != nil && ultimoNomeField.text != nil && nacionalidadeField.text != nil {
                SP1010Repository.repository.pessoas.append(Pessoa(primeiroNome: primeiroNomeField.text!, ultimoNome: ultimoNomeField.text!, nacionalidade: nacionalidadeField.text!))
                displayAlert(message: "Nova pessoa inserida", buttonTitle: "OK", vc: self)
            }
        }
        SP1010Repository.repository.savePessoas()
        
    }
    
    func displayAlert(message: String, buttonTitle: String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: buttonTitle, style: .default, handler: nil)
        alertController.addAction(OKAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //estou a mostrar uma pessoa que já foi inserida
        if let p = pessoa {
            primeiroNomeField.text = p.primeiroNome
            ultimoNomeField.text = p.ultimoNome
            nacionalidadeField.text = p.nacionalidade
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
