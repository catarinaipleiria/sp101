//
//  PessoasTableViewController.swift
//  SP101
//
//  Created by Catarina Silva on 08/03/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import UIKit

class PessoasTableViewController: UITableViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var index = 0
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        //print("\(SP1010Repository.repository.pessoas[0].primeiroNome)")
        
        GreetingClient.fetchGreet()
        
        GeonamesClient.fetchCountry(countryCode: "BR", completion: { (country) in
            if country == nil {
                print ("Resultado vazio")
            } else {
                print("nome: \(country!.name)")
                print("capital: \(country!.capital)")
                print("população: \(country!.population)")
            }
        })
        
        let defaults = UserDefaults.standard
        defaults.set(120, forKey: Constants.VALUE)
        
        let valor = defaults.integer(forKey: Constants.VALUE)
        
        print("valor: \(valor)")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return SP1010Repository.repository.pessoas.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "pessoaCell")
        
        cell.textLabel?.text = "\(SP1010Repository.repository.pessoas[indexPath.row].primeiroNome)" + " " + "\(SP1010Repository.repository.pessoas[indexPath.row].ultimoNome)"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pessoa = SP1010Repository.repository.pessoas[indexPath.row]
        index = indexPath.row
        self.performSegue(withIdentifier: "detalhesPessoaSegue", sender: pessoa)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "detalhesPessoaSegue" {
            if let controller = segue.destination as? PessoaViewController {
                controller.pessoa = sender as? Pessoa
                controller.index = index
            }
        }
    }
    

}
