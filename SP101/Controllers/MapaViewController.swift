//
//  MapaViewController.swift
//  SP101
//
//  Created by Catarina Silva on 15/03/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapaViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    let locationManager = CLLocationManager()
    var tappedLocation:CLLocationCoordinate2D?
    
    var currentLocation = CLLocationCoordinate2D() {
        didSet {
            mapa.region = MKCoordinateRegionMake(currentLocation, MKCoordinateSpanMake(0.01, 0.01))
        }
    }
    
    var annotation = MKPointAnnotation()
    
    
    @IBOutlet weak var mapa: MKMapView!
    
    
    @IBAction func shareButtonPressed(_ sender: UIBarButtonItem) {
        var text = "Não sei onde vou..."
        
        if let loc = tappedLocation {
            text = "Vou para \(loc.latitude), \(loc.longitude)"
        }
        let activityViewController = UIActivityViewController(activityItems: [ text ], applicationActivities: nil)
        //para ipad:
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func goToTapPoint(_ sender: UIButton) {
        
        //if let loc = tappedLocation {
        let placemark: MKPlacemark = MKPlacemark(coordinate: tappedLocation!, addressDictionary: nil)
        let mapItem:MKMapItem = MKMapItem(placemark: placemark)
        mapItem.name = "tapped Location"
        //}
        
//        let launchOptions:NSDictionary = NSDictionary(object:MKLaunchOptionsDirectionsModeDriving, forKey:MKLaunchOptionsDirectionsModeKey as NSCopying)
//
//        let currentLocationMapItem:MKMapItem = MKMapItem.forCurrentLocation()
//        MKMapItem.openMaps(with: [currentLocationMapItem, mapItem], launchOptions: launchOptions as? [String : AnyObject])
        
        showAlertMaps(to: tappedLocation!, destinationName: "tapped")
        
    }
    
    func showAlertMaps(to: CLLocationCoordinate2D, destinationName: String){
        let choose = UIAlertController(title: "", message: "Abrir em", preferredStyle: .actionSheet)
        if UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL) {
            let google = UIAlertAction(title: "Google Maps", style: .default, handler:  { action in
                self.openGoogleMapsWithDirections(to: to)
            })
            choose.addAction(google)
        }
        
        let apple = UIAlertAction(title: "Apple Maps", style: .default, handler:  { action in
            
            self.openMapsAppWithDirections(to: to, destinationName: destinationName)
        })
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        choose.addAction(apple)
        choose.addAction(cancel)
        present(choose, animated: true, completion: nil)
    }
    
    func openGoogleMapsWithDirections(to coordinate: CLLocationCoordinate2D){
        if UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL) {
            UIApplication.shared.openURL(NSURL(string:"comgooglemaps://?saddr=&daddr=\(coordinate.latitude),\(coordinate.longitude)")! as URL)
        }
    }
    
    func openMapsAppWithDirections(to coordinate: CLLocationCoordinate2D, destinationName name: String) {
        let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name
        mapItem.openInMaps(launchOptions: options)
    }
    
    @IBAction func tappedScreen(_ sender: UITapGestureRecognizer) {
        let tapPoint = sender.location(in: mapa)
        tappedLocation = mapa.convert(tapPoint, toCoordinateFrom: mapa)
        annotation.coordinate = tappedLocation!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //configguration of locationManager
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        //configuration of map
        mapa.delegate = self
        
//        let span = MKCoordinateSpanMake(0.01, 0.01)
//        let region = MKCoordinateRegionMake(currentLocation, span)
//        mapa.region = region
        
        mapa.region = MKCoordinateRegionMake(currentLocation, MKCoordinateSpanMake(0.01, 0.01))
        
        mapa.showsUserLocation = true
        annotation.coordinate = currentLocation
        annotation.title = "Localização atual"
        mapa.addAnnotation(annotation)
    }


    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coord = locationObj?.coordinate
        if let c = coord {
            print("latitude: \(c.latitude)")
            print("longitude: \(c.longitude)")
            currentLocation = c
            //mapa.region = MKCoordinateRegionMake(currentLocation, MKCoordinateSpanMake(0.01, 0.01))
            //annotation.coordinate = c
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
