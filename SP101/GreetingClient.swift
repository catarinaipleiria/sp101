//
//  GreetingClient.swift
//  SP101
//
//  Created by Catarina Silva on 12/04/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import Foundation

class GreetingClient{
    static func fetchGreet(){ //}-> (Int,String) {
        if let url = URL(string: "http://rest-service.guides.spring.io/greeting") {
            let dataDask = URLSession.shared.dataTask(with: url, completionHandler: {(data,response,error) in
                if data == nil {
                    //tratamento de erros
                    return
                }
                //se chega aqui data não é nil
                if let jsonDict = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                    if let id = jsonDict["id"] {
                        print("id: \(id)")
                    }
                    if let content = jsonDict["content"] {
                        print("content: \(content)")
                    }
                }
            })
            dataDask.resume()
            return
        }
    }
}
