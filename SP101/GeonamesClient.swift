//
//  GeonamesClient.swift
//  SP101
//
//  Created by Catarina Silva on 12/04/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import Foundation

class GeonamesClient{
    //http://api.geonames.org/countryInfoJSON?lang=en&country=FR&username=desenvolvimentoswift
    static func fetchCountry(countryCode:String, completion:@escaping (Country?)->Void){
        if let url = URL(string: "http://api.geonames.org/countryInfoJSON?lang=en&country=\(countryCode)&username=desenvolvimentoswift") {
            //print(url)
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data,response,error) in
                if data == nil {
                    completion(nil)
                    return
                }
                if let jsonDict = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any] {
                    if let geonamesArray = jsonDict?["geonames"] as? [[String:Any]]{
                        for countryJSON in geonamesArray {
                            let country = Country.parse(json: countryJSON)
                            completion(country)
                            return
                        }
                    }
                }
            })
            dataTask.resume()
            return
        }
    }
}
