//
//  SP101Repository.swift
//  SP101
//
//  Created by Catarina Silva on 19/04/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import Foundation

class SP1010Repository {
 
    static let repository = SP1010Repository()
    
    private init() {}
    
    var pessoas = [Pessoa]()
    
    func savePessoas() {
        print("saving \(pessoas.count) persons to: \(Constants.ArchivePersons)")
        
        NSKeyedArchiver.archiveRootObject(pessoas, toFile: Constants.ArchivePersons)
    }
    
    func getPessoas() {
    
        if let p = NSKeyedUnarchiver.unarchiveObject(withFile: Constants.ArchivePersons) as? [Pessoa] {
            pessoas = p
            print("got \(pessoas.count) persons")
        }
        
        
    }
}
