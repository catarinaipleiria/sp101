//
//  Country.swift
//  SP101
//
//  Created by Catarina Silva on 12/04/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import Foundation

struct Country {
    let name:String
    let population:Int
    let capital:String
    
    static func parse(json:[String:Any]) -> Country? {
        if let name = json["countryName"] as? String  {
            if let populationString = json["population"] as? String{
                if let population = Int(populationString){
                    if let capital = json["capital"] as? String {
                        return Country(name: name, population: population, capital: capital)
                    }
                }
            }
        }
        print("Erro no parse do país")
        return nil
    }
}
