//
//  Pessoa.swift
//  SP101
//
//  Created by Catarina Silva on 08/03/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import Foundation

class Pessoa: NSObject, NSCoding {
    var primeiroNome : String = " "
    var ultimoNome : String = " "
    var nacionalidade : String = "Portugal"
    
    init(primeiroNome:String, ultimoNome:String, nacionalidade:String){
        self.primeiroNome = primeiroNome
        self.ultimoNome = ultimoNome
        self.nacionalidade = nacionalidade
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(primeiroNome, forKey: Constants.PRIMEIRO_NOME)
        aCoder.encode(ultimoNome, forKey: Constants.ULTIMO_NOME)
        aCoder.encode(nacionalidade, forKey: Constants.NACIONALIDADE)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.primeiroNome = aDecoder.decodeObject(forKey: Constants.PRIMEIRO_NOME) as! String
        self.ultimoNome = aDecoder.decodeObject(forKey: Constants.ULTIMO_NOME) as! String
        self.nacionalidade = aDecoder.decodeObject(forKey: Constants.NACIONALIDADE) as! String
    }
    
}
