//
//  Constants.swift
//  SP101
//
//  Created by Catarina Silva on 19/04/2018.
//  Copyright © 2018 ipleiria. All rights reserved.
//

import Foundation

struct Constants {
    static let VALUE = "valor"
    static let PRIMEIRO_NOME = "primeiro nome"
    static let ULTIMO_NOME = "ultimo nome"
    static let NACIONALIDADE = "nacionalidade"
    
    //ARCHIVING DIR
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    
    //File for archiving persons
    static let ArchivePersons = Constants.DocumentsDirectory.appendingPathComponent("persons.archive").path
    
    static var DocumentsDirectory2 = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!

    
    
}
